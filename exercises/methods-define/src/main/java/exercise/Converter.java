package exercise;

class Converter {
    // BEGIN
    public static int convert(int zVolume, String zUnit) {
        switch (zUnit) {
            case "Kb": return zVolume / 1024;
            case "b": return zVolume * 1024;
            default: return 0;
        }
    }
    public static void main(String[] args) {
        int volume = 10;
        String unit = "b"; // хардкодить плохо, поэтому задаем значения для вывода задания для класса converter через переменные
        System.out.println(volume + " Kb = " + Converter.convert(volume, unit) + " b");
    }
    // END
}
