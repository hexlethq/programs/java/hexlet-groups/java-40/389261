package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int zSideA, int zSideB, int zAngle) {
        double zAngleRad = zAngle * Math.PI / 180; // конвертируем угол в радианы
        double zSquare = zSideA * zSideB * Math.sin(zAngleRad) / 2; // считаем площадь треугольника
        return zSquare;
    }
    public static void main(String[] args) {
        System.out.println(Triangle.getSquare(4, 5, 45));
    }
    // END
}
