package exercise;

// Точка — один из простых геометрических примитивов, который можно расположить на плоскости.
// Её положение определяется двумя координатами, и в математике она записывается так: (2, 3),
// где первое число — координата по оси x, а второе — по оси y.
// В этом задании нужно создать абстракцию для работы с точкой.
// Вам предстоит самостоятельно принять решение, о том, как она будет организована.

class Point {
    // BEGIN
//    makePoint() — принимает на вход координаты и возвращает точку.
    public static int[] makePoint(int x, int y) {
        int[] point = {x, y};
        return point;
    }

    //    getX() — принимает на вход точку и возвращает координату по оси x.
    public static int getX(int[] point) {
        return point[0];
    }

    //    getY() — принимает на вход точку и возвращает координату по оси y.
    public static int getY(int[] point) {
        return point[1];
    }

    //    pointToString() — принимает на вход точку и возвращает её строковое представление.
    public  static String pointToString(int[] point) {
        return "("+ point[0] + ", " + point[1] + ")";
    }

    //    getQuadrant() — принимает на вход точку и возвращает номер квадранта, в котором эта точка расположена.
//    Если точка не принадлежит ни одному квадранту (лежит на оси координат), то функция должна возвращать 0.
    public static int getQuadrant(int[] point) {
        int quadrant = 0;
        if (point[0] > 0 && point[1] > 0) quadrant = 1;
        if (point[0] < 0 && point[1] > 0) quadrant = 2;
        if (point[0] < 0 && point[1] < 0) quadrant = 3;
        if (point[0] > 0 && point[1] < 0) quadrant = 4;
        return quadrant;
    }

    // Самостоятельная работа

    // Реализуйте публичный статический метод getSymmetricalPointByX().
    // Метод принимает в качестве аргумента точку и возвращает новую точку, симметричную исходной относительно оси Х.
    public static int[] getSymmetricalPointByX(int[] point) {
        int[] point2 = new int[2];
        point2[0] = point[0];
        point2[1] = -point[1];
        return point2;
    }

    // Реализуйте публичный статический метод calculateDistance().
    // Метод принимает в качестве аргументов две точки и возвращает расстояние между этими точками.
    public static int calculateDistance(int[] point1, int[] point2) {
        int deltaX = Math.abs(point2[0] - point1[0]);
        int deltaY = Math.abs(point2[1] - point1[1]);
        double distance = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
        return (int) distance;
    }
    // END
}
