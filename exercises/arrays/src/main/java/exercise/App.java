package exercise;

class App {
    // BEGIN
//    Создайте публичный статический метод reverse(), который принимает
//    в качестве аргумента массив целых чисел и возвращает новый массив целых чисел
//    с элементами, расположенными в обратном порядке.
//    Если передан пустой массив, метод должен вернуть пустой массив.
        public static int[] reverse(int[] zArray) {
            if (zArray == null) return zArray;
            int[] newArray = new int[zArray.length];
            int j = 0;
            for (int i = zArray.length - 1; i >= 0 ; i--) {
                newArray[j] = zArray[i];
                j++;
            }
            return newArray;
        }

//        Создайте публичный статический метод mult().
//        Метод принимает в качестве аргумента массив целых чисел и возвращает
//        произведение всех элементов массива.

        public static int mult(int[] zArray) {
            int zMult = 1;
            for (int num: zArray) {
                zMult *= num;
            }
            return zMult;
        }

//        Самостоятельная работа
//        Реализуйте публичный статический метод flattenMatrix().
//        Метод принимает в качестве аргумента матрицу целых чисел (двумерный массив)
//        и записывает все её элементы в одномерный массив.
//        Если на вход передана пустая матрица, метод должен вернуть пустой массив.

        public static int[] flattenMatrix(int[][] zArray) {
            int newArray[] = new int[zArray.length * zArray[1].length];
            int k = 0;
            for (int i = 0; i < zArray.length; i++) {
                for (int j = 0; j < zArray[i].length; j++) {
                    newArray[k] = zArray[i][j];
                    k++;
                }
            }
            return newArray;
        }

    // END
}
