package exercise;

import java.util.Arrays;
import java.util.ArrayList;


// BEGIN
public class Kennel {
    static class Puppy {
        public String name;
        public String breed;

        public Puppy(String name, String breed) {
            this.name = name;
            this.breed = breed;
        }
    }

    private static ArrayList<Puppy> puppies = new ArrayList<>();

    //    addPuppy() — принимает в качестве аргумента щенка и добавляет его в питомник.
    public static void addPuppy(String[] newPuppy) {
        Puppy puppy = new Puppy(newPuppy[0], newPuppy[1]);
        puppies.add(puppy);

    }

    //    addSomePuppies() — принимает в качестве аргумента массив щенков и добавляет их в питомник.
    public static void addSomePuppies(String[][] newPuppies) {
        for (String[] newPuppy : newPuppies) {
            Puppy puppy = new Puppy(newPuppy[0], newPuppy[1]);
            puppies.add(puppy);
        }
    }

    //    getPuppyCount() — возвращает общее количество щенков, находящихся на данный момент в питомнике.
    public static int getPuppyCount() {
        return puppies.size();
    }

    //    isContainPuppy() — принимает в качестве аргумента имя щенка и проверяет,
//    есть ли в питомнике щенок с таким именем. Если щенок есть, метод возвращает true,
//    в ином случае — false.
    public static Boolean isContainPuppy(String puppyName) {
        for (Puppy puppy : puppies) {
            if (puppy.name.equals(puppyName)) return true;
        }
        return false;
    }

    //    getAllPuppies() — возвращает всех щенков, имеющихся в питомнике,
//    в виде вложенного массива (смотрите пример вызова метода).
//    Щенки в массиве должны располагаться в том порядке, в каком они были добавлены в питомник.
    public static String[][] getAllPuppies() {
        String[][] allPuppies = new String[puppies.size()][2];
        for (int i = 0; i < puppies.size(); i++) {
            allPuppies[i][0] = puppies.get(i).name;
            allPuppies[i][1] = puppies.get(i).breed;
        }
        return allPuppies;
    }

    //    getNamesByBreed() — принимает в качестве аргумента породу и
//    возвращает массив с именами щенков этой породы.
    public static String[] getNamesByBreed(String breed) {
        ArrayList<String> names = new ArrayList<>();
        for (Puppy puppy : puppies) {
            if (puppy.breed.equals(breed)) {
                names.add(puppy.name);
            }
        }
        return names.toArray(new String[0]);
    }

    //    resetKennel() — очищает питомник, не оставляя в нем ни одного щенка.
    public static void resetKennel() {
        puppies.clear();
    }

    // Самостоятельная работа
// Создайте публичный статический метод removePuppy(),
// который позволяет выдавать щенка из питомника новому хозяину.
// Метод принимает в качестве аргумента имя щенка.
// Если щенок с таким именем есть в питомнике, метод удаляет его из питомника
// и возвращает true. Если щенок отсутствует, метод просто возвращает false.
    public static Boolean removePuppy(String name) {
        for (Puppy puppy : puppies) {
            if (puppy.name.equals(name)) {
                puppies.remove(puppy);
                return true;
            }
        }
        return false;
    }
}
// END
