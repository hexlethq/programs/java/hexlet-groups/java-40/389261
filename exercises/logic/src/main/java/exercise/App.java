package exercise;

class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable = false;
        if (number >= 1001 && number % 2 == 1)  {
            isBigOddVariable = true;
        }
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        System.out.println(number % 2 == 0 ? "yes" : "no");
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        if (minutes < 15 && minutes >=0) {
            System.out.println("First");
        } else if (minutes > 14 && minutes < 30) {
            System.out.println("Second");
        } else if (minutes > 29 && minutes < 45) {
            System.out.println("Third");
        } else if (minutes > 44 && minutes < 60) {
            System.out.println("Fourth");
        } else
            System.out.println("The number is out of range!");
        // END
    }
}
