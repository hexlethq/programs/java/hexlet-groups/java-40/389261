package exercise;

//Создайте публичный статический метод getTypeOfTriangle(), который определяет тип треугольника.
//Метод принимает три аргумента — длины трех отрезков и определяет возможность существования треугольника,
//составленного из этих отрезков. Если такой треугольник существует, то возвращает его тип в виде строки:
//"Разносторонний", "Равнобедренный" или "Равносторонний". Если треугольник не существует,
//метод должен вернуть строку "Треугольник не существует".


class App {
    // BEGIN
    public static String getTypeOfTriangle(int sideA, int sideB, int sideC) {
        if ((sideA >= sideB + sideC) || (sideC >= sideB + sideA) || (sideB >= sideA + sideC)) {
            return "Треугольник не существует";
        } else if ((sideA == sideB) && (sideB == sideC) && (sideA == sideC)) {
            return "Равносторонний";
        } else if ((sideA == sideB) || (sideB == sideC) || (sideA == sideC)) {
            return "Равнобедренный";
        } else {
            return "Разносторонний";
        }
        // END
    }
}
