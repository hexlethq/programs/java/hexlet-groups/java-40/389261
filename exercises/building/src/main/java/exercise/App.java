// BEGIN
package exercise;
import com.google.gson.Gson;

class App {
    public static void main(String Args[]) {
        System.out.println("Hello, World!");
    }

//    Самостоятельная работа
//    В файле build.gradle подключите в качестве зависимости библиотеку google-gson.
//    В классе App создайте публичный статический метод toJson(). Метод принимает
//    в качестве аргумента массив строк и возвращает JSON представление массива в виде строки.
//    Для преобразования массива в JSON воспользуйтесь методами класса Gson из подключенной библиотеки.
    public static String toJson(String[] string) {
        Gson gson = new Gson();
        String jsonArray = gson.toJson(string);
        return jsonArray;
    }
}
// END
