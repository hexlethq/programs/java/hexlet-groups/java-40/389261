package exercise;

import javax.print.event.PrintJobListener;

class App {
    public static void numbers() {
        // BEGIN
        byte a, b, c, d;
        a = 8;
        b = 2;
        c = 100;
        d = 3;
        System.out.print(a / b + c % d);
        // END
    }

    public static void strings() {
        String language = "Java";
        // BEGIN
        System.out.print(language + " " + "works " + "on " + "JVM");
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        System.out.print(soldiersCount + " " + name);
        // END
    }
}