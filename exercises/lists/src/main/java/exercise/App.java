// Реализуйте публичный статический метод scrabble(), который принимает на вход два параметра:
// набор символов для составления слова в нижнем регистре (в виде строки) и слово.
// Метод проверяет, можно ли из переданного набора составить это слово. В результате вызова функция возвращает true или false.

// При проверке учитывается количество символов, нужных для составления слова и
// не учитывается их регистр (заглавные и строчные символы считаются одинаковыми).
package exercise;

import java.util.ArrayList;
import java.util.List;

// BEGIN
public class App {
    public static Boolean scrabble(String symbols, String word) {

        if (symbols.equals("") || word.equals("")) {
            return false;
        }

        List<Character> symbolsToList = new ArrayList<>();
        for (char symbol : symbols.toCharArray()) {
            symbolsToList.add(Character.toLowerCase(symbol));
        }

        List<Character> wordToList = new ArrayList<>();
        for (char symbol : word.toCharArray()) {
            wordToList.add(Character.toLowerCase(symbol));
        }

        int counter = 0;
        for (Character symbol : wordToList) {
            if (symbolsToList.contains(symbol)) {
                counter++;
                symbolsToList.remove(symbol);
            }
        }

        return counter == wordToList.size();

    }
}


//END
