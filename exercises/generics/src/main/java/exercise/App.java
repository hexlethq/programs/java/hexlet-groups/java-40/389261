package exercise;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Map.Entry;

// BEGIN
public class App {

//    Создайте класс Appс публичным статическим методом findWhere(), который принимает следующие аргументы:
//    Список List книг. Каждая книга представляет собой словарь Map, в котором ключи и значения представлены строками
//    Словарь Map из пар ключей и значений, представленных строками
//    Метод должен возвращать список List со всеми книгами, данные которых соответствуют всем переданным парам.
//    Если совпадений нет, метод должен вернуть пустой список.


    public static List<Map<String, String>> findWhere(List<Map<String, String>> books, Map<String, String> where) {
        List<Map<String, String>> resultList = new ArrayList<Map<String, String>>();

        for (Map<String, String> book : books) {
            int i = 0;
            for (String key : where.keySet()) {
                if (book.get(key).equals(where.get(key))) {
                    i++;
                }
                if (i == where.keySet().size()) resultList.add(book);
            }
        }
        return resultList;
    }
}
//END
