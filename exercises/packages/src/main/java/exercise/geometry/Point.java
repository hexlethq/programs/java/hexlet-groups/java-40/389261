// BEGIN
package exercise.geometry;
public class Point {

    public static double[] makePoint(double x, double y) {
        double[] point = {x, y};
        return point;
    }

    public static double getX(double[] point) {
        return point[0];
    }

    public static double getY(double[] point) {
        return point[1];
    }

    //    берем метод из одного из прошлых домашних заданий:
    //    getQuadrant() — принимает на вход точку и возвращает номер квадранта, в котором эта точка расположена.
//    Если точка не принадлежит ни одному квадранту (лежит на оси координат), то функция должна возвращать 0.
    public static int getQuadrant(double[] point) {
        int quadrant = 0;
        if (point[0] > 0 && point[1] > 0) quadrant = 1;
        if (point[0] < 0 && point[1] > 0) quadrant = 2;
        if (point[0] < 0 && point[1] < 0) quadrant = 3;
        if (point[0] > 0 && point[1] < 0) quadrant = 4;
        return quadrant;
    }

}


// END