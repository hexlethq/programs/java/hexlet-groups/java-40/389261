// BEGIN
package exercise.geometry;
import java.util.Arrays;

public class Segment {
    //    makeSegment() — принимает на вход две точки и возвращает отрезок.
    public static double[][] makeSegment(double[] point1, double[] point2) {
        double[][] segment = new double[2][2];
        segment[0] = Arrays.copyOf(point1, 2);
        segment[1] = Arrays.copyOf(point2, 2);
        return segment;
    }

    //    getBeginPoint() — принимает на вход отрезок и возвращает точку начала отрезка.
    public static double[] getBeginPoint(double[][] segment) {
        double point[] = segment[0];
        return point;
    }

    //    getEndPoint() — Принимает на вход отрезок и возвращает точку конца отрезка.
    public static double[] getEndPoint(double[][] segment) {
        double point[] = segment[1];
        return point;
    }
}
// END
