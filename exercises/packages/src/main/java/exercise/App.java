// BEGIN
package exercise;
import java.util.Arrays;
import exercise.geometry.Point;
import exercise.geometry.Segment;

public class App {
    //    getMidpointOfSegment() — принимает в качестве аргумента отрезок и возвращает точку середины отрезка.
    public static double[] getMidpointOfSegment(double[][] segment) {
        double x = (Point.getX(Segment.getBeginPoint(segment)) + Point.getX(Segment.getEndPoint(segment)))/2;
        double y = (Point.getY(Segment.getBeginPoint(segment)) + Point.getY(Segment.getEndPoint(segment)))/2;
        return Point.makePoint(x, y);
    }

    //    reverse() — принимает в качестве аргумента отрезок
//    и возвращает новый отрезок с точками, добавленными
//    в обратном порядке (begin меняется местами с end).
//    Точки в результирующем отрезке должны быть копиями (по значению)
//    соответствующих точек исходного отрезка. То есть они не должны быть ссылкой
//    на одну и ту же точку, так как это разные точки (пускай и с одинаковыми координатами).
    public static double[][] reverse(double[][] segment) {
        return Segment.makeSegment(Segment.getEndPoint(segment), Segment.getBeginPoint(segment));
    }


//    Самостоятельная работа
//    В классе App создайте публичный статический метод isBelongToOneQuadrant().
//    Метод принимает на вход отрезок и определяет, лежит ли весь отрезок целиком в одном квадранте.
//    Если начало и конец отрезка лежат в одном квадранте, метод возвращает true, иначе — false.
//    Если хотя бы одна из точек лежит на оси координат, то считается, что отрезок не находится целиком в одном квадранте.
    public static boolean isBelongToOneQuadrant(double[][] segment) {
        if (Point.getQuadrant(Segment.getBeginPoint(segment)) == Point.getQuadrant(Segment.getEndPoint(segment))) return true;
        return false;
    }


}

// END
