package exercise;

import java.util.*;
import java.text.SimpleDateFormat;
import java.text.ParseException;

class App {
    // BEGIN
    //    Создайте публичный статический метод buildList(), который принимает на вход массив строк
    //    и преобразует его в маркированный html список. Если передан пустой массив, метод должен
    //    возвращать пустую строку. Для сборки строки используйте класс StringBuilder.
    //    Не забудьте про отступы и переносы строк. Пример html, который должен получится, приведен ниже.
    public static String buildList(String[] animals) {
        StringBuilder resultHTML = new StringBuilder();
        if (animals.length == 0) return "";
        resultHTML.append("<ul>");
        for (String animal : animals) {
            resultHTML.append(getStringToTag(animal, "li"));
        }
        resultHTML.append("\n</ul>");
        return resultHTML.toString();
    }

    // вспомогательный метод для форматирования строк в теги
    private static String getStringToTag(String text, String tag) {
        return String.format("\n  <%s>%s</%s>", tag, text, tag);
    }

    //    Создайте публичный статический метод getUsersByYear(), который принимает
    //    в качестве аргументов массив пользователей и год.
    //    Каждый пользователь представлен массивом строк, в котором
    //    первый элемент — это имя пользователя, а второй — дата рождения.
    //    Метод должен возвращать маркированный html список
    //    с именами пользователей, которые родились в указанный год.
    public static String getUsersByYear(String[][] users, int year) throws ParseException {
        String[] userList = getUsers(users, year);
        if (userList.length == 0) {
            return "";
        } else return buildList(userList);
    }

    //вспомогательный метод: получаем год из элемента массива
    private static int getYear(String[] user) throws ParseException {
        Calendar date = new GregorianCalendar();
        date.setTime(parseDate(user[1], "yyyy-mm-dd"));
        return date.get(Calendar.YEAR);
    }

    // вспомогательный метод: получаем из исходного массива массив пользователей, родившихся в заданный год
    private static String[] getUsers(String[][] users, int year) throws ParseException {
        String[] usersByYear = new String[users.length];
        int counter = 0;
        for (String[] user : users) {
            if (getYear(user) == year) {
                usersByYear[counter] = user[0];
                counter++;
            }
        }
        return Arrays.copyOfRange(usersByYear, 0, counter);
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
//    В классе уже создан публичный статический метод getYoungestUser(),
//    который принимает в качестве аргументов массив пользователей и дату в виде строки формата "3 Sep 1975".
//    Допишите этот метод. Метод должен возвращать имя самого младшего из пользователей,
//    которые родились до указанной даты. В примере ниже только четыре пользователя родились
//    до 11 июля 1989 года. Это пользователи с именами "John Smith", "Vanessa Vulf", "Alice Lucas" и "Elsa Oscar".
//    Самый младший из них — пользователь "John Smith", так как он родился позже остальных в этой группе.
//    Если никто из пользователей не родился до указанной даты, метод должен вернуть пустую строку.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        String youngestUser = "";
        Date startDate = parseDate(date, "d MMM yyyy");
        Date candidateBirthDate = parseDate("01-01-1900", "dd-mm-yyyy"); // дата рождения "кандидата" в младшие юзеры
        for (String[] user : users) {
            if (parseDate(user[1], "yyyy-mm-dd").before(startDate) && parseDate(user[1], "yyyy-mm-dd").after(candidateBirthDate)) {
                youngestUser = user[0];
                candidateBirthDate = parseDate(user[1], "yyyy-mm-dd");
            }
        }
        return youngestUser;
        // END
    }

    // вспомогательный метод, который парсит дату из строки по изменяемому шаблону
    private static Date parseDate(String date, String pattern) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern, Locale.ENGLISH);
        return formatter.parse(date);
    }
}
