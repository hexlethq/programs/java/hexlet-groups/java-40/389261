package exercise;

// Создайте публичный статический метод sort(),
// который сортирует переданный массив целых чисел,
// используя алгоритм пузырьковой сортировки.

class App {
    // BEGIN
    static int[] sort(int[] array) {
        if (array.length == 0) return array;
        for (int j = 1; j < array.length; j++) {
            for (int i = 0; i < array.length - j; i++) {
                if (array[i] > array[i + 1]) {
                    int tmp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = tmp;
                }
            }
        }
        return array;
    }
    // END
}
