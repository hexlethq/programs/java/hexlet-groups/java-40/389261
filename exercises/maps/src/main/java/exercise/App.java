package exercise;


import java.util.HashMap;
import java.util.Map;

// BEGIN

//Создайте класс App с публичным статическим методом getWordCount(), который принимает
// в качестве аргумента предложение. Метод возвращает словарь Map, в котором ключи —
// это слова исходного предложения, а значения — количество соответствующих слов в предложении.
// Слова в предложении разделены пробелами.
public class App {

    public static Map<String, Integer> getWordCount(String sentence) {
        Map<String, Integer> sentenceMap = new HashMap<>();
        String[] words = sentence.split(" ");
        if (sentence == "") return sentenceMap;
        for(String word : words){
            if (sentenceMap.containsKey(word)) {
                sentenceMap.put(word, sentenceMap.get(word) + 1);
            } else sentenceMap.put(word, 1);
        }
        return sentenceMap;
    }

    //В классе App создайте публичный статический метод toString(),
    // который принимает в качестве аргумента словарь Map с количеством слов в предложении.
    // Метод должен возвращать строковое представление словаря. Обратите внимание на форматирование вывода.
    // Каждое слово выводится с новой строки с отступом в 2 пробела.

    public static String toString(Map<String, Integer> sentenceMap) {
        if (sentenceMap.size() == 0) return "{}";
        String result = "{\n";
        for (String key : sentenceMap.keySet()) {
            Integer value = sentenceMap.get(key);
            result += String.format("  %s: %d\n", key, value);
        }
        return result + "}";
    }

}
//END
