package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String str) {
        if (str == "") return "Ошибка! Введите непустую строку"; //напрямую в задании и в тестах это не указано, но стоит привыкать
        str = str.trim(); // убираем лишние пробелы в начале и в конце
        String modifiedStr = "" + str.charAt(0); // незамысловато конвертируем первый символ в строку и делаем его же первым элементов аббревиатуры
        for (int i = 2; i < str.length(); i++) {
            if (str.charAt(i - 1) == ' ' &&  str.charAt(i) != ' ') { // если предыдущий символ пробел, то следующий включаем в аббревиатуру
                modifiedStr += str.charAt(i);
            }
        }
        return modifiedStr.toUpperCase(); // поднимаем регистр при выводе
    }
    // END
}
