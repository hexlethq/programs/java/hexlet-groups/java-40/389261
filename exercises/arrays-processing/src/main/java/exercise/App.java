package exercise;

class App {
    // BEGIN

    // Создайте публичный статический метод getIndexOfMaxNegative(), который принимает в качестве аргумента массив целых
    // чисел и возвращает индекс максимального отрицательного элемента массива. Если в массиве несколько таких одинаковых
    // элементов, нужно вернуть индекс первого из них. Если передан пустой массив или в массиве нет отрицательных
    // элементов, метод должен вернуть -1.

    public static int getIndexOfMaxNegative(int[] arrInput) {
        if (arrInput.length == 0) return -1; // проверяем на пустой массив
        if (arrInput[getMinIndex(arrInput)] > 0) return -1; // проверка что массив не состоит только из положительных элементов
        int index = getMinIndex(arrInput); // предположим за стартовое значение самый минимальный элемент массива
        for (int i = 0; i < arrInput.length; i++) {
            index = arrInput[i] > arrInput[index] && arrInput[i] < 0 ? i : index;
        }
        return index;
    }

    // Создайте публичный статический метод getElementsLessAverage(). Метод принимает на вход массив из
    // целых чисел и возвращает новый массив, состоящий из элементов исходного массива.
    // Каждый элемент нового массива должен быть не больше среднего арифметического всех элементов исходного массива.
    // Если передан пустой массив, метод должен вернуть пустой массив.

    public static int[] getElementsLessAverage(int[] arrInput) {
        if (arrInput.length == 0) return arrInput; // проверяем на пустой массив
        int[] arrOutput = new int[getElementsCount(arrInput)]; // инициализируем новый массив, длину считаем вспомогательным методом
        int average = getArrayAverage(arrInput); // пишем в переменную чтобы цикл каждый раз не обращался к вспомогательному методу
        int j = 0;
        for (int i = 0; i < arrInput.length; i++) {
            if (arrInput[i] <= average) {
                arrOutput[j] = arrInput[i];
                j++;
            }}
        return arrOutput;
    }

    // Самостоятельная работа
    // Создайте публичный статический метод getSumBeforeMinAndMax().
    // Метод принимает на вход массив из целых чисел и возвращает сумму элементов, которые расположены в массиве между
    // минимальным и максимальным элементами массива (не включая сами минимальный и максимальный элементы).

    public static int getSumBeforeMinAndMax(int[] arrInput) {
        int sum = 0;
        int i = getMaxIndex(arrInput);
        int j = getMinIndex(arrInput);
        for (int k = Math.min(i,j) + 1; k < Math.max(i,j); k++) {
            sum += arrInput[k];
        }
        return sum;
    }

    // БЛОК ВСПОМОГАТЕЛЬНЫХ МЕТОДОВ

    static int getMaxIndex(int[] arrInput) { // вывел в отдельные методы получение индексов макс./мин. элементов массива
        int maxIndex = 0;
        for (int i = 0; i < arrInput.length; i++) {
            maxIndex = arrInput[i] > arrInput[maxIndex] ? i : maxIndex;
        }
        return maxIndex;
    }

    static int getMinIndex(int[] arrInput) {
        int minIndex = 0;
        for (int i = 0; i < arrInput.length; i++) {
            minIndex = arrInput[i] < arrInput[minIndex] ? i : minIndex;
        }
        return minIndex;
    }


    static int getArrayAverage(int[] arrInput) { // метод который считает среднее арифметическое массива
        int average = 0;
        for (int i = 0; i < arrInput.length; i++) {
            average = average + arrInput[i];
        }
        return average / arrInput.length;
    }

    static int getElementsCount(int[] arrInput) { // метод который считает сколько эдементов в массиве меньше среднего арифметического массива
        int count = 0;
        int average = getArrayAverage(arrInput);
        for (int i = 0; i < arrInput.length; i++) {
            if (arrInput[i] <= average) count++;
        }
        return count;
    }
    // END
}
